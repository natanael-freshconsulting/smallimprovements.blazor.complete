using System;
using System.Collections.Generic;
using FluentAssertions;
using SmallImprovements.General.Data;
using SmallImprovements.General.Dtos;
using Xunit;

namespace SmallImprovements.General.Tests
{
    public class FakeStorageTests
    {
        [Fact]
        public void TeamsSanityCheck()
        {
            var mock = new SmallImprovementsRepository();

            for (var i = 0; i < 4; i++)
            {
                var team = mock.Invoking(m => m.Teams[i])
                    .Should().NotThrow()
                    .Subject;

                team.Name.Should().NotBeEmpty();
                team.Id.Should().NotBe(Guid.Empty);
                team.Creation.Should()
                    .BeAfter(new DateTimeOffset(2020, 1, 1, 0, 0, 0, TimeSpan.Zero));
            }
        }

        [Fact]
        public void PeopleSanityCheck()
        {
            var mock = new SmallImprovementsRepository();

            for (var i = 0; i < 10; i++)
            {
                var person = mock.Invoking(m => m.People[i])
                    .Should().NotThrow()
                    .Subject;

                person.Name.Should().NotBeEmpty();
                person.Team.Should().NotBeNull();
                person.Id.Should().NotBe(Guid.Empty);
                person.Creation.Should()
                    .BeAfter(new DateTimeOffset(2020, 1, 1, 0, 0, 0, TimeSpan.Zero));
            }
        }
    }
}