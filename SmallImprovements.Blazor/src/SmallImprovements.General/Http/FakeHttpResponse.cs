using System.Net.Http;

namespace SmallImprovements.General.Http
{
    public class FakeHttpResponse
    {
        public FakeHttpResponse(string json = "")
        {
            Content = new FakeHttpResponseContent(json);
        }

        public FakeHttpResponseContent Content { get; }
    }
}