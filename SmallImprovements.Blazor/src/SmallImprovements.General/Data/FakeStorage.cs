using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using SmallImprovements.General.Dtos;

namespace SmallImprovements.General.Data
{
    public class FakeStorage
    {
        private static readonly Random RandomGenerator = new Random();

        protected static readonly List<TeamDto> TeamsList = new List<TeamDto>
        {
            new TeamDto { Name = "Design" },
            new TeamDto { Name = "Devs" },
            new TeamDto { Name = "Finances" },
            new TeamDto { Name = "HR" }
        };

        protected static readonly List<PersonDto> PeopleList =
            Enumerable.Range(0, 10)
                .Select(_ => new PersonDto
                {
                    Name = $"{GetRandomFirstName()} {GetRandomLastName()}",
                    Team = TeamsList[GetRandomNumber(TeamsList.Count)]
                })
                .ToList();

        protected static readonly List<ObjectiveDto> ObjectivesList =
            Enumerable.Range(0, 100)
                .Select(_ => new ObjectiveDto
                {
                    Owner = PeopleList[GetRandomNumber(PeopleList.Count)],
                    Title = GetRandomTitle(),
                    Description = GetRandomDescription(),
                    Progress = GetRandomObjectiveProgress(),
                    Creation = GetRandomDate()
                })
                .ToList();

        protected static readonly List<PraiseDto> PraisesList =
            Enumerable.Range(0, 100)
                .Select(_ =>
                {
                    var (from, to) = GetRandomFromTo();
                    return new PraiseDto
                    {
                        From = from,
                        To = to,
                        Title = GetRandomTitle(),
                        Description = GetRandomDescription(),
                        Creation = GetRandomDate()
                    };
                })
                .ToList();

        protected static readonly List<ActivityDto> ActivitiesList =
            ObjectivesList.Cast<ActivityDto>()
            .Union(PraisesList.Cast<ActivityDto>())
            .ToList();

        private static (PersonDto from, PersonDto to) GetRandomFromTo()
        {
            var fromIdx = GetRandomNumber(PeopleList.Count);
            var toIdx = -1;

            do
            {
                toIdx = GetRandomNumber(PeopleList.Count);
            }
            while (toIdx == fromIdx);

            return (PeopleList[fromIdx], PeopleList[toIdx]);
        }

        private static int GetRandomNumber(int max)
        {
            return RandomGenerator.Next(0, max);
        }

        private static string GetRandomFirstName()
        {
            // https://www.ssa.gov/oact/babynames/
            var fnames = new[]
            {
                "Liam",
                "Noah",
                "William",
                "James",
                "Oliver",
                "Benjamin",
                "Emma",
                "Olivia",
                "Ava",
                "Isabella",
                "Sophia",
                "Charlotte",
                "Elijah",
                "Mia",
                "Lucas",
                "Amelia",
                "Mason",
                "Harper",
                "Logan",
                "Evelyn"
            };

            return fnames[GetRandomNumber(fnames.Length)];
        }

        private static string GetRandomLastName()
        {
            // https://www.al.com/news/2019/10/50-most-common-last-names-in-america.html
            var lnames = new[]
            {
                "Smith",
                "Johnson",
                "Williams",
                "Brown",
                "Jones",
                "Garcia",
                "Miller",
                "Davis",
                "Rodriguez",
                "Martinez",
                "Hernandez",
                "Lopez",
                "Gonzalez",
                "Wilson",
                "Anderson",
                "Thomas",
                "Taylor",
                "Moore",
                "Jackson",
                "Martin"
            };

            return lnames[GetRandomNumber(lnames.Length)];
        }

        private static string GetRandomTitle()
        {
            // https://lipsum.com/
            var titleString =
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit. " +
                "Pellentesque dignissim lacinia diam faucibus vestibulum. Sed feugiat varius quam, " +
                "vel pretium leo auctor vulputate. Maecenas nec auctor lectus. In in consequat sem. Proin" +
                " id tortor id tortor dignissim efficitur. Duis feugiat pretium lacus ac volutpat. Nunc ipsum " +
                "metus, malesuada at mauris sit amet, ultrices rhoncus sem. In tristique metus arcu, rhoncus" +
                " ullamcorper justo posuere fermentum. Nunc massa turpis, imperdiet nec pellentesque id, pharetra " +
                "sed nunc. Nam consequat quis magna vitae accumsan. Curabitur sagittis, erat rhoncus " +
                "condimentum bibendum, velit tellus vulputate lectus, in malesuada velit orci vel ante. " +
                "Integer vel imperdiet nisi. Nulla tincidunt enim enim, a aliquet urna ullamcorper id. " +
                "Proin ex urna, mattis ac erat at, porttitor dictum lacus.";

            var startIdx = RandomGenerator.Next(0, titleString.Length - 50);
            return titleString.Substring(startIdx, 49);
        }

        private static string GetRandomDescription()
        {
            // http://fillerama.io/
            var descriptions =
                " ▲   Your eyes can deceive you. Don't trust them. Hokey religions and ancient weapons are no match " +
                "for a good blaster at your side, kid. Oh God, my uncle. How am I ever gonna explain this? I don't " +
                "know what you're talking about. I am a member of the Imperial Senate on a diplomatic mission to " +
                "Alderaan--  In my experience, there is no such thing as luck. No! Alderaan is peaceful. We have" +
                " no weapons. You can't possibly… Dantooine. They're on Dantooine. Ye-ha! Dantooine. They're on " +
                "Dantooine.  What?! Your eyes can deceive you. Don't trust them. I suggest you try it again, " +
                "Luke. This time, let go your conscious self and act on instinct. The more you tighten your grip, " +
                "Tarkin, the more star systems will slip through your fingers.  Dantooine. They're on Dantooine. " +
                "In my experience, there is no such thing as luck. What good is a reward if you ain't around to " +
                "use it? Besides, attacking that battle station ain't my idea of courage. It's more like…suicide." +
                "  Ye-ha! As you wish. A tremor in the Force. The last time I felt it was in the presence of my " +
                "old master. Don't be too proud of this technological terror you've constructed. The ability to " +
                "destroy a planet is insignificant next to the power of the Force.  I care. So, what do you think of" +
                " her, Han? Kid, I've flown from one side of this galaxy to the other. I've seen a lot of strange " +
                "stuff, but I've never seen anything to make me believe there's one all-powerful Force controlling " +
                "everything. There's no mystical energy field that controls my destiny. It's all a lot of simple " +
                "tricks and nonsense.  Red Five standing by. What!? You mean it controls your actions? The more you" +
                " tighten your grip, Tarkin, the more star systems will slip through your fingers. Escape is not" +
                " his plan. I must face him, alone.  The plans you refer to will soon be back in our hands. The " +
                "plans you refer to will soon be back in our hands. Don't act so surprised, Your Highness. You " +
                "weren't on any mercy mission this time. Several transmissions were beamed to this ship by Rebel" +
                " spies. I want to know what happened to the plans they sent you.  He is here. Hey, Luke!" +
                " May the Force be with you. Look, I can take you as far as Anchorhead. You can get a transport" +
                " there to Mos Eisley or wherever you're going. I have traced the Rebel spies to her. Now " +
                "she is my only link to finding their secret base.  No! Alderaan is peaceful. We have no weapons. " +
                "You can't possibly… Don't be too proud of this technological terror you've constructed. " +
                "The ability to destroy a planet is insignificant next to the power of the Force.  Look, I ain't " +
                "in this for your revolution, and I'm not in it for you, Princess. I expect to be well paid." +
                " I'm in it for the money. She must have hidden the plans in the escape pod. Send a detachment" +
                " down to retrieve them, and see to it personally, Commander. There'll be no one to stop us this time!" +
                "  I call it luck. I call it luck. Still, she's got a lot of spirit. I don't know, what do you think? " +
                "Hokey religions and ancient weapons are no match for a good blaster at your side, kid.  Hey, Luke!" +
                " May the Force be with you. I care. So, what do you think of her, Han? Escape is not his plan. I " +
                "must face him, alone. I'm surprised you had the courage to take the responsibility yourself.  Look," +
                " I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you're " +
                "going. All right. Well, take care of yourself, Han. I guess that's what you're best at, ain't it? " +
                " What?! I care. So, what do you think of her, Han? I call it luck. Obi-Wan is here. The Force is" +
                " with him.  Kid, I've flown from one side of this galaxy to the other. I've seen a lot of strange " +
                "stuff, but I've never seen anything to make me believe there's one all-powerful Force controlling " +
                "everything. There's no mystical energy field that controls my destiny. It's all a lot of simple " +
                "tricks and nonsense. A tremor in the Force. The last time I felt it was in the presence of my" +
                " old master.  Ye-ha! Don't underestimate the Force. I don't know what you're talking about. " +
                "I am a member of the Imperial Senate on a diplomatic mission to Alderaan-- Dantooine. They're " +
                "on Dantooine. What good is a reward if you ain't around to use it? Besides, attacking that " +
                "battle station ain't my idea of courage. It's more like…suicide.  I'm trying not to, kid." +
                " I want to come with you to Alderaan. There's nothing for me here now. I want to learn the" +
                " ways of the Force and be a Jedi, like my father before me. What!? What?!  Partially, but" +
                " it also obeys your commands. I have traced the Rebel spies to her. Now she is my only link" +
                " to finding their secret base. Remember, a Jedi can feel the Force flowing through him. I " +
                "have traced the Rebel spies to her. Now she is my only link to finding their secret base. " +
                " Look, I ain't in this for your revolution, and I'm not in it for you, Princess." +
                " I expect to be well paid. I'm in it for the money. " +
                "Oh God, my uncle. How am I ever gonna explain this? You're all clear, kid. Let's blow this thing and go home!";

            var startIdx = RandomGenerator.Next(0, descriptions.Length - 500);
            return descriptions.Substring(startIdx, 499);
        }

        private static ObjectiveProgress GetRandomObjectiveProgress()
        {
            var progresses = Enum.GetValues(typeof(ObjectiveProgress));
            return (ObjectiveProgress)progresses.GetValue(GetRandomNumber(progresses.Length));
        }

        private static DateTime GetRandomDate()
        {
            var years = RandomGenerator.Next(0, 2);
            var months = RandomGenerator.Next(0, 11);
            var days = RandomGenerator.Next(0, 31);

            return DateTime.Now.AddYears(-years).AddMonths(-months).AddDays(-days);
        }
    }
}