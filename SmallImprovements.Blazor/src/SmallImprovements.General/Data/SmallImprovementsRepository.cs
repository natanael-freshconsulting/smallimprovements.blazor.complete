using System.Collections.Generic;
using System.Linq;
using SmallImprovements.General.Dtos;

namespace SmallImprovements.General.Data
{
    public class SmallImprovementsRepository : FakeStorage
    {
        public List<TeamDto> Teams => TeamsList;
        public List<PersonDto> People => PeopleList;
        public List<ActivityDto> Activities => ActivitiesList;
    }
}