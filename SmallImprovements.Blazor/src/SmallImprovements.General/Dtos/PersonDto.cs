using System;

namespace SmallImprovements.General.Dtos
{
    public class PersonDto : BaseDto
    {
        public PersonDto()
        {
            Email = "admin@admin.com";
            Password = "admin";
        }

        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public TeamDto Team { get; set; }
    }
}