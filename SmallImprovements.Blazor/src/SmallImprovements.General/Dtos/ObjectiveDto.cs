using System;
using System.Net.NetworkInformation;

namespace SmallImprovements.General.Dtos
{
    public enum ObjectiveProgress
    {
        Open,
        InProgress,
        Achieved,
        NotAchieved
    }

    public class ObjectiveDto : ActivityDto
    {
        public ObjectiveProgress Progress { get; set; }
        public PersonDto Owner { get; set; }
        public override ActivityType ActivityType => ActivityType.Objective;
    }

    public static class ObjectiveProgressExtensions
    {
        public static string EnumToString(this ObjectiveProgress progress)
        {
            switch (progress)
            {
                case ObjectiveProgress.InProgress: return "In progress";
                case ObjectiveProgress.NotAchieved: return "Not achieved";
                default: return progress.ToString();
            }
        }
    }
}