using System;

namespace SmallImprovements.General.Dtos
{
    public class TeamDto : BaseDto
    {
        public string Name { get; set; }
    }
}