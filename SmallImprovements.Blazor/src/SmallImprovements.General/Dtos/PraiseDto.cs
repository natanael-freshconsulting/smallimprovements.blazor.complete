using System;

namespace SmallImprovements.General.Dtos
{
    public class PraiseDto : ActivityDto
    {
        public PersonDto From { get; set; }
        public PersonDto To { get; set; }

        public override ActivityType ActivityType => ActivityType.Praise;
    }
}