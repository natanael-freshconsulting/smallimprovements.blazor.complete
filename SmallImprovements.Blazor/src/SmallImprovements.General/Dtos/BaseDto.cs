using System;

namespace SmallImprovements.General.Dtos
{
    public abstract class BaseDto
    {
        protected BaseDto()
        {
            Id = Guid.NewGuid();
            Creation = DateTimeOffset.Now;
        }

        public Guid Id { get; set; }
        public DateTimeOffset Creation { get; set; }
    }
}