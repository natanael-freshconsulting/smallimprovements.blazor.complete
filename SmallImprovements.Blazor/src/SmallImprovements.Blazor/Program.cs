using System;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Blazored.LocalStorage;
using SmallImprovements.Blazor.Services;

namespace SmallImprovements.Blazor
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var builder = WebAssemblyHostBuilder.CreateDefault(args);
            builder.RootComponents.Add<App>("app");

            var services = builder.Services;
            services.AddScoped(sp => new HttpClient { BaseAddress = new Uri(builder.HostEnvironment.BaseAddress) });

            services
                .AddScoped<ActivityState>()
                .AddScoped<UserState>()
                .AddScoped<IUsersService, UsersService>()
                .AddScoped<IActivitiesService, ActivitiesService>();

            services.AddBlazoredLocalStorage();
            await builder.Build().RunAsync();
        }
    }
}
