using System;
using System.Threading.Tasks;

namespace SmallImprovements.Blazor.Services
{
    public class ActivityState
    {
        public event Func<Task> OnActivityCreated;

        public async Task ActivityCreated()
        {
            await OnActivityCreated();
        }
    }
}