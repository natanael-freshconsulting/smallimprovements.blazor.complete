﻿using SmallImprovements.General.Dtos;
using SmallImprovements.General.Http;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace SmallImprovements.Blazor.Services
{
    public class UsersService : IUsersService
    {
        private readonly UserState _userState;

        public UsersService(UserState userState)
        {
            _userState = userState;
        }

        public async Task<List<PersonDto>> GetAll()
        {
            using var client = new FakeHttpClient();
            var response = await client.GetAsync("person");
            var json = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<List<PersonDto>>(json);
        }

        public async Task Login(string email, string password)
        {
            using var client = new FakeHttpClient();
            var response = await client.GetAsync($"person?email={email}");
            var json = await response.Content.ReadAsStringAsync();

            var loggedUser = JsonConvert.DeserializeObject<PersonDto>(json);
            await _userState.SetUser(loggedUser);
        }

        public Task Logout()
        {
            return _userState.SetUser(null);
        }
    }
}